package com.bms.accountdetails.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bms.accountdetails.customizedexceptions.JsonExceptions;
import com.bms.accountdetails.service.Accountservice;

import net.sf.json.JSONObject;


@RestController
@RequestMapping("api/v1/account")
public class Acountcontroller {
	
	@Autowired
	private Accountservice service;
	
	@PutMapping(value="/updatedtls", consumes = "application/json")
	public Boolean updateAccountdtls(@RequestBody JSONObject jsondata) throws JsonExceptions {
		return service.updateAccountdtls(jsondata);
	}

}
