package com.bms.accountdetails.customizedexceptions;

public class JsonExceptions extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JsonExceptions(String message) {
		super(message);
	}
}
