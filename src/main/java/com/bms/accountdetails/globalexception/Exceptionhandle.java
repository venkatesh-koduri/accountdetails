package com.bms.accountdetails.globalexception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.bms.accountdetails.customizedexceptions.JsonExceptions;


@RestControllerAdvice
public class Exceptionhandle {

	@ExceptionHandler(value = JsonExceptions.class)
	public Map<String, Object> handleExceptions(JsonExceptions je){
		Map<String, Object> excMap = new HashMap<String, Object>();
		excMap.put("msg", je.getMessage());
		excMap.put("timestamp", java.time.LocalDateTime.now());
		excMap.put("REQ_SATAUS", HttpStatus.BAD_REQUEST);
		return excMap;
	}
}
