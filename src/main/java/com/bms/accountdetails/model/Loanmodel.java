package com.bms.accountdetails.model;

public class Loanmodel {
	
	private Integer id;
	private String loantype;
	private Double loanamt;
	private Float roi;
	private Integer dol;
	private Integer customerid;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLoantype() {
		return loantype;
	}
	public void setLoantype(String loantype) {
		this.loantype = loantype;
	}
	public Double getLoanamt() {
		return loanamt;
	}
	public void setLoanamt(Double loanamt) {
		this.loanamt = loanamt;
	}
	public Float getRoi() {
		return roi;
	}
	public void setRoi(Float roi) {
		this.roi = roi;
	}
	public Integer getDol() {
		return dol;
	}
	public void setDol(Integer dol) {
		this.dol = dol;
	}
	public Integer getCustomerid() {
		return customerid;
	}
	public void setCustomerid(Integer customerid) {
		this.customerid = customerid;
	}
	
}
