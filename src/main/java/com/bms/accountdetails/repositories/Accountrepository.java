package com.bms.accountdetails.repositories;

import org.springframework.data.repository.CrudRepository;

import com.bms.accountdetails.entity.Loanentity;


public interface Accountrepository extends CrudRepository<Loanentity, Integer> {

}
