package com.bms.accountdetails.service;

import com.bms.accountdetails.customizedexceptions.JsonExceptions;

import net.sf.json.JSONObject;

public interface Accountservice {

	Boolean updateAccountdtls(JSONObject jsondata) throws JsonExceptions;
}
