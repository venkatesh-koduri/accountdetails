package com.bms.accountdetails.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bms.accountdetails.customizedexceptions.JsonExceptions;
import com.bms.accountdetails.entity.Loanentity;
import com.bms.accountdetails.model.Loanmodel;
import com.bms.accountdetails.repositories.Accountrepository;
import com.bms.accountdetails.service.Accountservice;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.sf.json.JSONObject;

@Service
public class Accountserviceimpl implements Accountservice {
	
	@Autowired
	private Accountrepository repo;

	@Override
	public Boolean updateAccountdtls(JSONObject jsondata) throws JsonExceptions {

		boolean iscreated = true;
		try {
			Loanmodel model = new ObjectMapper().readValue(jsondata.toString(), Loanmodel.class);
			Loanentity entity = prepareloanentity(model);
			repo.save(entity);
		} catch (JsonProcessingException e) {
			iscreated = false;
			throw new JsonExceptions("Json Processing Exception");
		}
		return iscreated;
	}

	private Loanentity prepareloanentity(Loanmodel model) {
		Loanentity entity = new Loanentity();
		entity.setDol(model.getDol());
		entity.setLoanamt(model.getLoanamt());
		entity.setLoantype(model.getLoantype());
		entity.setRoi(model.getRoi());
		entity.setCustomerid(model.getCustomerid());
		entity.setId(model.getId());
		return entity;
	}

}
