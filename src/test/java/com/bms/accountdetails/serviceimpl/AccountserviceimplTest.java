package com.bms.accountdetails.serviceimpl;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.bms.accountdetails.customizedexceptions.JsonExceptions;
import com.bms.accountdetails.entity.Loanentity;
import com.bms.accountdetails.repositories.Accountrepository;

import net.sf.json.JSONObject;

class AccountserviceimplTest {

	@Mock
	private Accountrepository repo;
	
	@InjectMocks
	private Accountserviceimpl serviceimpl;
	
	
	@BeforeEach
	private void init() {
	   MockitoAnnotations.openMocks(this);

	}
	

	@Test()
	public void updateAccountdtlsTest() throws JsonExceptions {
		
		when(repo.save(new Loanentity())).thenReturn(new Loanentity());
		JSONObject jo = new JSONObject();
		jo.put("loantype", "CAR LOAN");
		jo.put("loanamt", 700000);
		jo.put("roi", 6);
		jo.put("dol", 36);
		jo.put("customerid", 1);
		jo.put("id", 1);
		assertEquals(true, serviceimpl.updateAccountdtls(jo));
	}
	
	
	@Test()
	public void updateAccountdtlsTestException() {
		
		when(repo.save(new Loanentity())).thenReturn(new Loanentity());
		JSONObject jo = new JSONObject();
		jo.put("loantype", "CAR LOAN");
		jo.put("loanamt", 700000);
		jo.put("roi", "dsfddfds");
		jo.put("dol", 36);
		jo.put("customerid", 1);
		jo.put("id", 1);
		Exception exp = assertThrows(JsonExceptions.class, () -> serviceimpl.updateAccountdtls(jo));
		assertEquals("Json Processing Exception", exp.getMessage());
	}

}
